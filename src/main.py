#! /bin/python3
"""main module for alie project"""

import qutip.states as st
from qutip.qip import hadamard_transform
from qutip.qip.gates import cnot
from qutip.qip.gates import gate_expand_1toN as gate_x
from qutip.qip.qubits import tensor

NQBITS = 1

def init_state(nqbits):
    l = [0 for i in range(nqbits)]
    return st.ket(l), st.ket([1])

def oracle_balanced(state, out): #should be considered balanced fun is 1 if weak qubit is 1
    g = cnot(NQBITS + 1, NQBITS-1, NQBITS)
    ret = tensor(state, out).transform(g)
    return ret

def oracle_const(state, out):
    return tensor(state, out) #tensor(state, out)

oracle = oracle_const

def Deutch_Jozsa():
    init, dummy = init_state(NQBITS)
    h = hadamard_transform(NQBITS)
    h2 = hadamard_transform(1)
    state =  init.transform(h)
    d = h2 * dummy
#    print(state)
    #do your thing with oracle

    newstate = oracle(state, d)

    test = newstate.extract_states(range(NQBITS))

    h1 = gate_x(h, NQBITS + 1, 0)
    newstate = newstate.transform(h1)
    print(newstate)
    print(newstate[0] ** 2 + newstate[1] **2)

if __name__ == "__main__":
    Deutch_Jozsa()
